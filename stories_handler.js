var pIndex = 0;
var pArr;
var alreadyStoryChoice = false;
var index = 0;

// Controllo di inserimento " se il campo è vuoto..."
function controllaHomeInteract() {
    if ($("#form").val().length == 0) {
        alert("Errore: non puoi lasciare il campo vuoto");
    } else {
        nozione(storia.missioni[missioniIndex].nozione);
    }
}

// Stampa a video un testo lettera per lettera
function consoleText(words, id) {
    var letterCount = 1;
    var target = document.getElementById(id);
    target.setAttribute('style', 'color: #fff');
    window.setInterval(function () {

        target.innerHTML = words[0].substring(0, letterCount);
        letterCount += 1;
    }, 10) //una lettera ogni 10ms

}

function preparaArray() {
    array = splitStory(storia.missioni[missioniIndex].storia);
}

// Svuota la pagina e prepara la missione successiva
function nextStory() {
    clearMain();
    clearFooter();
    missioniIndex++;
    alreadyPop = false;
    if (storia.missioni[missioniIndex].tipo != "only_nozione") {
        index = 0;
        story_end = false;
        preparaArray();
        setBGImage();
        var storybox = '<div class="row "><div class="col-12" onclick="printConsoleText()""><p id="p1"></p></div></div>';
        $("#main").append(storybox);
        printConsoleText();
    }
    else {
        storyTypeChoice();
    }
}

function setBGImage() {
    $("#main").css("background-image", "url('Images/" + storia.missioni[missioniIndex].background + "')");
}

// prende ciò che l'utente ha inserito
function getValue() {
    return $("#form").val();
}

// Ritorna un array dove ogni elemento è un pezzo di storia fino a 50 caratteri.
function splitStory(s) {
    var arr = [];
    var cont = 0;
    var index = 0;
    var index2 = 0;
    var tmp = s.split(""); //splitta ogni lettera
    for (var i = 0; i < tmp.length; i++) {
        if (i == tmp.length - 1) {
            arr[cont] = s.substring(index, i);
        }
        else if (i > (index + 50) && (tmp[i] == "." || tmp[i] == "!" || tmp[i] == ":" || tmp[i] == "?")) {
            index2 = i;
            arr[cont] = s.substring(index, index2 + 1);
            index = index2 + 1;
            cont++;
        }
    }
    return arr;
}

// Ritorna un array dove ogni elemento è un pezzo di storia fino a 20 caratteri.
function splitPopUp(p) {
    var arr = [];
    var cont = 0;
    var index = 0;
    var index2 = 0;
    var tmp = p.split("");
    for (var i = 0; i < tmp.length; i++) {
        if (i == tmp.length - 1) {
            arr[cont] = p.substring(index, i);
        }
        else if (i > (index + 20) && (tmp[i] == "." || tmp[i] == "!" || tmp[i] == ":" || tmp[i] == "?")) {
            index2 = i;
            arr[cont] = p.substring(index, index2 + 1);
            index = index2 + 1;
            cont++;
        }
    }
    return arr;
}

// prepara il paragrafo e il testo da stampare
function printConsoleText() {
    if (index < array.length) {
        $("#text").remove(); // span dove inseriamo il testo
        $("#p1").append("<span id='text'></span>"); // paragrafo nello storybox
        consoleText([array[index]], 'text');
        index++;
    }
    else {
        // Se il popup non è undefined
        if (storia.missioni[missioniIndex].popup) {
            if (alreadyPop == false) {
                getPopUp(storia.missioni[missioniIndex]);
            }
        }
        else {
            if (alreadyPop == false) {
                storyTypeChoice();
            }
        }
    }
};

// selettore del tipo di missione
function storyTypeChoice() {
    alreadyPop = true;
    switch (storia.missioni[missioniIndex].tipo) {
        case "only_history":
            only_history(storia.missioni[missioniIndex]);
            break;
        case "open_quest":
            open_quest(storia.missioni[missioniIndex]);
            break;
        case "closed_quest":
            closed_quest(storia.missioni[missioniIndex]);
            break;
        case "click_on_obj":
            click_on_obj(storia.missioni[missioniIndex]);
            break;
        case "home_interact":
            home_interact(storia.missioni[missioniIndex]);
            break;
        case "final_recap":
            final_recap(storia.missioni[missioniIndex]);
            break;
        case "end":
            end();
            break;
        case "only_nozione":
            only_nozione(storia.missioni[missioniIndex]);
            break;
    }
}

// Setta il titolo della storia
function printTitle(t) {
    title = t.toString();
    var num = title.split(".");
    $("title").text("Storia " + num[0]);
}

//Gestione popup
function getPopUp(mission) {
    pIndex = 0;
    alreadyStoryChoice = false;
    if (mission.popup) {
        alreadyPop = true;
        var popup = '<div class="row" style="position:absolute; bottom: 0; width: 100%; margin-bottom:1vh" onclick="printPopUpText()"><div id="popUp" class="col-2" style="left:3vw"></div><div class="col-9 dialogpopup" style="left:3vw" ><p id="popup_text"></p></div></div>';
        var popup_img = '<img class="dialogpopupimg" src="Images/' + mission.popup.img + '">'
        $("#main").append(popup);
        $("#popUp").append(popup_img);
        pArr = splitPopUp(mission.popup.text);
        printPopUpText();
    }
}

// Stampa il testo del popup
function printPopUpText() {
    if (pIndex < pArr.length) {
        $("#pText").remove();
        $("#popup_text").append("<span id='pText'></span>");
        consoleText([pArr[pIndex]], 'pText');
        pIndex++;
    }
    else {
        if (!alreadyStoryChoice) {
            storyTypeChoice();
            alreadyStoryChoice = true;
        }
    }
}

function clearMain() {
    $("#main").empty();
}

//gestione tipo click_on_obj
function click_on_obj(mission) {
    printQuestion(mission.interazione); //domanda
    var obj = '<img src="Images/' + mission.obj + '" style="position:absolute; left:' + mission.position.left + '; top:' + mission.position.top + '; height:' + mission.position.height + '; width:' + mission.position.width + ';" onclick="nozione(storia.missioni[missioniIndex].nozione)"></img>'
    $("#main").append(obj);
}

//gestione only_history
function only_history(mission) {
    nozione(mission.nozione);
}

//gestione closed_quest
function closed_quest(mission) {
    printQuestion(mission.quest.domanda); //domanda
    for (let i = 0; i < mission.quest.opzioni.length; i++) {
        if (mission.quest.opzioni[i] == mission.quest.corretta) {
            var btn = '<div id="footcol" class="col"><input id="btm-true" type="button" value="' + mission.quest.opzioni[i] + '" onclick="true_answer()"></div>' //onclick ricompensa + skip missione
        }
        else {
            var btn = '<div id="footcol" class="col"><input id="btm-false" class="falseBtn", type="button" value="' + mission.quest.opzioni[i] + '" onclick="false_answer()"></div>' //onclick penalità
        }
        $("#footer_row").append(btn);
    }

}

function true_answer() {
    $("#btm-true").css("background-color", "green");
    setTimeout(function () { clearMain(); clearFooter(); nozione(storia.missioni[missioniIndex].nozione); }, 1000); //aspetta 1 secondo prima di passare alla nozione 
    assegnaPunteggio(bonus);
}

function false_answer() {
    $(".falseBtn").css("background-color", "red");
    assegnaPunteggio(malus);
}

function open_quest(mission) {
    printQuestion(mission.quest.domanda); //domanda
    var form = '<input type="text" id="form" name="form" placeholder="Inserisci la risposta">';
    var btn = '<input id="sendform" type="button" value="Invia" onclick="controlloInserimento(getValue(), storia.missioni[missioniIndex].quest.risposta)">';
    $("#footer_row").append(form);
    $("#footer_row").append(btn);
}

function controlloInserimento(risposta, valide) { //valide = array di risposte corrette
    var giusta = false;
    for (let i = 0; i < valide.length; i++) {
        if (risposta == valide[i]) {
            giusta = true;
        }
    }
    if (giusta) {
        setTimeout(function () { clearMain(); clearFooter(); nozione(storia.missioni[missioniIndex].nozione); }, 1000);
        assegnaPunteggio(bonus);
    }
    else {
        alert("Risposta sbagliata");
        assegnaPunteggio(malus);
    }
}

function home_interact(mission) {
    printQuestion(mission.interazione.todo);
    var form = '<input type="text" id="form" name="form" placeholder="Inserisci la risposta">';
    var btn = '<input id="sendform" type="button" value="Invia" onclick="controllaHomeInteract()">';
    $("#footer_row").append(form);
    $("#footer_row").append(btn);
}

//Gestisce i passaggi caratterizzati da solo una nozione
function only_nozione(mission) {
    nozione(mission.nozione);
}

// Pagina finale che mostra il risultato
function end(mission) {
    if (storia.ricompensa.tipo == "downgrade") {
        var questionbox = '<div class="row"><div class="dialogbox"><p>' + points + "/" + storia.ricompensa.num + '<img class="icon" src="Images/' + storia.ricompensa.img + '" style="padding-bottom: 0.5vh;"></p></div></div>';
        $("#main").append(questionbox);
    }
    else {
        var questionbox = '<div class="row"><div class="dialogbox"><p>' + points + '<img class="icon" src="Images/' + storia.ricompensa.img + '" style="padding-bottom: 0.5vh;"></p></div></div>';
        $("#main").append(questionbox);
    }
}

// Stampa la domanda
function printQuestion(text) {
    var questionbox = '<div class="row"><div class="dialogbox"><p id="question">' + text + '</p></div></div>';
    $("#main").append(questionbox);
}

function clearFooter() {
    $("#footer_row").empty();
}

// Gestisce la nozione e l'avanzamento alla missione successiva
function nozione(nozione) {
    if (nozione) {
        clearMain();
        $("#main").css("background-image", "url('Images/" + storia.background_nozione + "')");
        var row = '<div id="nozione" class="row"><div class="col-12" style="text-align:center"><br><h5><b>LO SAPEVI CHE...<b></h5></div></div>';
        $("#main").append(row);
        var img = '<div class="col-12 " style="text-align:center;height:50vw"><img src="Images/' + nozione.img + '" alt=""></div>';
        $("#nozione").append(img);
        for (let i = 0; i < nozione.text.length; i++) {
            var temp = '<div class="col-12"><p style="font-size: 2vh;font-weight:bold;margin-bottom:0.8vh">' + nozione.text[i] + '</p></div>';
            $("#nozione").css("color", storia.color_nozione);
            $("#nozione").append(temp);
        }
        var arrowBtn = '<div class="col-12" style="text-align:right"><img class="icon" src="Images/rightArrow.png" onclick="nextStory()" alt="next"></div>';
        $("#footer_row").append(arrowBtn);
    }
    else {
        nextStory();
    }
}

// tiene aggiornato il punteggio attuale
function assegnaPunteggio(p) {
    var pp = parseInt(points); // num del json
    pp = pp + p;
    points = pp.toString(); // aggiunge bonus-malus
    $("#punteggio").remove();
    var ric = '<li id="punteggio"><span style="color: white; font-weight: bold; font-size: 2.5vh;">' + points + '</span><img class="icon" src="Images/' + storia.ricompensa.img + '" style="padding-bottom: 0.5vh;"></li>';
    $("#ul1").append(ric);
}

