
/* get json file name from url*/
function getUrlParameter(url) {
    var questionSplit = url.split('=');
    questionSplit.shift();
    printTitle(questionSplit);
    return questionSplit;
}

var numHistory = getUrlParameter(document.URL);
var jsonURL = 'JsonHistories/' + numHistory;

/* GET data from json*/
var tipoRicompensa;
var storia;
var array;
var missioniIndex = -1;
var story_end;
var points;
var bonus;
var malus;
var alreadyPop = false;

function callback(response) {

    storia = response;
    tipoRicompensa = response.ricompensa.tipo;
    points = response.ricompensa.num;
    bonus = parseInt(response.ricompensa.bonus);
    malus = parseInt(response.ricompensa.malus);
    console.log(storia);
    var ric = '<li id="punteggio"><span style="color: white; font-weight: bold; font-size: 2.5vh;">' + points + '</span><img class="icon" src="Images/' + storia.ricompensa.img + '" style="padding-bottom: 0.5vh;"></li>';
    $("#ul1").append(ric);
    nextStory();
}

function loadGame() {
    clearMain();
    $.ajax({
        url: jsonURL,
        dataType: 'json',
        success: function (data) {
            console.log(data);
            callback(data);
        },
        error: function (xhr, textstatus) {
            alert(textstatus + " " + xhr.status);
        }
    });
};
